#include "LifeMaterial.h"
#include "qopenglfunctions.h"

LifeMaterial::LifeMaterial(std::shared_ptr<QOpenGLShaderProgram> prog, int texunit, int num_lights)
    : Material(prog,num_lights),
      tex_unit(texunit),
      input_texture_id(-1)
{
}

void LifeMaterial::apply(unsigned int light_pass)
{
    // first do what Material needs to do (i.e. bind program, set animation time)
    Material::apply(light_pass);

    // bind input texture manually using its OpenGL ID
    QOpenGLFunctions gl(QOpenGLContext::currentContext());
    gl.glActiveTexture(GL_TEXTURE0 + tex_unit);
    gl.glBindTexture(GL_TEXTURE_2D, input_texture_id);
    prog_->setUniformValue("input_texture", tex_unit);

    // globals
    prog_->setUniformValue("dimension", dimension);
    prog_->setUniformValue("image_width", (GLint)image_size.width());
    prog_->setUniformValue("image_height", (GLint)image_size.height());
}


