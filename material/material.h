#pragma once

#include <QOpenGLShaderProgram>

#include <memory>

/*
 *   Interface for surface materials.
 *
 */
class Material
{
public:

    /*
     * constructor requires an existing shader program
     *
     */
    Material(std::shared_ptr<QOpenGLShaderProgram> prog, int num_lights = 1)
        :prog_(prog), lights(num_lights)
    {}

    // animation time
    float time = 0.0;

    /*
     *  apply: bind underlying shader program and set required uniforms
     *  This method needs to be overwritten / extended by the derived class.
     *
     *  light_pass is an optional argument so the shader may be called
     *  once for each light (or type of light).
     *
     */
    virtual void apply(unsigned int light_pass = 0);

    /*
     * returns the underlying OpenGL shader program object
     *
     */
    QOpenGLShaderProgram& program() const { return *prog_; }

    // vector of point lights used to light the material
    struct PointLight {
        QVector4D position_WC = QVector4D(0,1,5,1);
        QVector3D color = QVector3D(1,1,1);
        float intensity = 0.5;
    };

    std::vector<PointLight> lights;
protected:

    // reference to underlying shader program
    std::shared_ptr<QOpenGLShaderProgram> prog_;
};


