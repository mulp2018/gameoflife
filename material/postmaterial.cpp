#include "postmaterial.h"
#include "qopenglfunctions.h"

void
PostMaterial::apply(unsigned int)
{
    prog_->bind();

    // bind input texture manually using its OpenGL ID
    QOpenGLFunctions gl(QOpenGLContext::currentContext());
    gl.glActiveTexture(GL_TEXTURE0 + tex_unit);
    gl.glBindTexture(GL_TEXTURE_2D, input_texture_id);

    prog_->setUniformValue("input_texture", tex_unit);
    prog_->setUniformValue("image_width", (GLint)image_size.width());
    prog_->setUniformValue("image_height", (GLint)image_size.height());
    prog_->setUniformValue("kernel_width", (GLint)kernel_size.width());
    prog_->setUniformValue("kernel_height", (GLint)kernel_size.height());
    prog_->setUniformValue("use_antialiasing", use_antialiasing_);
}

