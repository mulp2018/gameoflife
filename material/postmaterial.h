#pragma once

#include "material.h"

class PostMaterial : public Material
{
public:

    // constructor requires existing shader program,
    // and to specify the tex unit to read from
    PostMaterial(std::shared_ptr<QOpenGLShaderProgram> prog,
                 int texunit) : Material(prog), tex_unit(texunit) {}

    // the texture to be post processed, get it from your FBO using the texture() method
    GLint input_texture_id;

    // the image size ("resolution") of the texture, needs to be set from outside
    QSize image_size;

    // kernel size of the filter to be applied
    QSize kernel_size = QSize(5,5);

    // use anti-aliasing?
    bool use_antialiasing_ = false;

    // texture unit to be used
    int tex_unit;

    // bind underlying shader program and set required uniforms
    void apply(unsigned int light_pass = 0) override;

};

