#pragma once

#include "material/phong.h"
#include <QVector3D>

class LifeMaterial : public Material
{
public:
    // constructor requires existing shader program
    LifeMaterial(std::shared_ptr<QOpenGLShaderProgram> prog, int texunit = 1, int num_lights=1);

    // bind underlying shader program and set required uniforms
    void apply(unsigned int light_pass = 0) override;

    // the dimension of the game
    QVector2D dimension;

    std::shared_ptr<QOpenGLTexture> initialTexture;

    // the texture to be post processed, get it from your FBO using the texture() method
    GLint input_texture_id;

    // the image size ("resolution") of the texture, needs to be set from outside
    QSize image_size;

    // texture unit to be used
    int tex_unit;

    void bindTexture() {
        program().bind();
        initialTexture->bind();
    }
};

