#ifndef GAMEOFLIFE_RENDERER_H
#define GAMEOFLIFE_RENDERER_H


#include "node.h"
#include "material/LifeMaterial.h"

#include <qopenglframebufferobject.h>

class GameOfLifeRenderer
{
public:
    GameOfLifeRenderer(std::shared_ptr<LifeMaterial> mat);

    // post-process image from an FBO and render to part of the screen.
    // assumes screen and FBO have same resolution.
    virtual void draw(QOpenGLFramebufferObject& fbo_in, QRect screen_mask);

    // for changing material parameters on the fly
    std::shared_ptr<LifeMaterial> material() const { return material_; }

protected:
    std::shared_ptr<LifeMaterial> material_;
    std::shared_ptr<Node> node_;
};

#endif // GAMEOFLIFE_RENDERER_H
