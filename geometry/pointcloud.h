#pragma once

#include "mesh/geometrybuffers.h"
#include <vector>

namespace geom {

/*
 *   An array of 3D points, to be rendered as points
 *
 */

class PointCloud : public GeometryBuffers
{
public:

    // copies point positions from 3D array; normals and texcoord are optional
    PointCloud(std::vector<QVector3D> points,
               std::vector<QVector3D> normals = {},
               std::vector<QVector2D> texcoords = {});
};

} // geom::

