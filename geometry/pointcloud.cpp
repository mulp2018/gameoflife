#include "pointcloud.h"
#include <assert.h>

using namespace std;

namespace geom {

PointCloud::PointCloud(std::vector<QVector3D> points,
                       std::vector<QVector3D> normals,
                       std::vector<QVector2D> texcoords
                       )
{
    // create OpenGL vertex buffer objects (VBOs) from geometry data
    assert(points.size() > 0);
    position_ = make_unique<VertexBuffer<QVector3D>>(points);
    if(normals.size()>0)
        normal_   = make_unique<VertexBuffer<QVector3D>>(normals);
    if(texcoords.size()>0)
        texcoord_ = make_unique<VertexBuffer<QVector2D>>(texcoords);

    // calculate bounding box from extreme vertices
    bbox_ = BoundingBox(points);

} // PointCloud()

} // geom::
