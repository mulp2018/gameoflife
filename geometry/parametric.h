#pragma once

#include <functional> // std::function
#include <math.h>

#include <QVector2D>
#include <QVector3D>

#include "mesh/geometrybuffers.h"

namespace geom {

/**
 * @brief The ParametricSurface class is a base class providing
 *        functionality to generate procedural (parametric) surfaces.
 *
 *        Each derived class simply needs to call generate() from
 *        their constructors, providing the appropriate ranges
 *        and functions in order to generate the desired surface.
 */

class ParametricSurface : public GeometryBuffers
{
public:


protected:

    /* generate() has to be called from within the constructor of the derived class (!)
     *
     * from, to: parameter range
     * patches_u, patches_v: number of patches / cells in each direction
     * pos: function generating position vector
     *      input:   QVector2D with parameter point
     *      returns: vertex position as QVector3D
     * norm: function generating normal vector, see pos
     *
     */
    void generate(QVector2D from, QVector2D to,
                  size_t patches_u, size_t patches_v,
                  std::function<QVector3D(float,float)> pos,
                  std::function<QVector2D(float,float)> tex = nullptr);

}; // ParametricSurface

/**
 * @brief Unit Sphere (radius 0.5) as parametric surface.
 */
class Sphere : public ParametricSurface {
public:
    // make unit sphere (radius 0.5, poles are on Z axis)
    Sphere(size_t patches_u, size_t patches_v);
};

/**
 * @brief Same as Sphere, except that texture coords are
 *        adapted to work with cylindrical projection maps
 *        (top of texture is mapped to one pole, bottom to the
 *         other pole)
 */
class Planet : public ParametricSurface {
public:
    Planet(size_t patches_u, size_t patches_v);
};

/**
 * @brief Torus: parametric surface with two radii
 */
class Torus : public ParametricSurface {
public:

    // make torus specified by two radii, winds around Z axis
    Torus(float r1, float r2, size_t patches_u, size_t patches_v);

};

/**
 * @brief Rect: rectangular part (-0.5 ... 0.5) of X-Z plane
 */
class Rect : public ParametricSurface {
public:

    Rect(size_t patches_u, size_t patches_v);

};

} // namespace geom
