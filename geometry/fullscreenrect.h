#pragma once

#include "parametric.h"

namespace geom {

/**
 * @brief Rect: full screen rectangle in XY plane from (-1,-1) to (1,1)
 */
class FullScreenRect : public ParametricSurface {
public:

    FullScreenRect();

};


} // namespace
