#include "fullscreenrect.h"

namespace geom {

FullScreenRect::FullScreenRect()
{

    auto pos = [](float s, float t) -> QVector3D {
        return QVector3D(-1 + s, -1 + t, 0 );
    };

    generate(QVector2D(0,0),
             QVector2D(2,2),
             1, 1,
             pos);

}

} // namespace
