#version 150

/*
 * Minimal Vertex Shader - only applies model-view-projection matrix
 * (C)opyright Hartmut Schirmacher, http://schirmacher.beuth-hochschule.de
 *
 */


// transformation matrices as provided by Cinder
uniform mat4 modelViewProjectionMatrix;

// uniform values from LifeMaterial
uniform vec2 dimension;
uniform sampler2D input_texture;
uniform int image_width;
uniform int image_height;

// in: position and normal vector in model coordinates (_MC)
in vec3 position_MC;
in vec2 texcoord;

void main(void) {
    
    // position to clip coordinates
    gl_Position = modelViewProjectionMatrix * vec4(position_MC,1);

}
