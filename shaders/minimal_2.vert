#version 150

/*
 * Minimal Vertex Shader - only applies model-view-projection matrix
 * (C)opyright Hartmut Schirmacher, http://schirmacher.beuth-hochschule.de
 *
 */


// transformation matrices as provided by Cinder
uniform mat4 modelViewProjectionMatrix;

// uniform values from LifeMaterial
uniform vec2 dimension;
uniform sampler2D input_texture;
uniform int image_width;
uniform int image_height;

// in: position and normal vector in model coordinates (_MC)
in vec3 position_MC;

out float pointSize;

out vec2 pointCoord;

void main(void) {
    

    // position to clip coordinates
    gl_Position = modelViewProjectionMatrix * vec4(position_MC,1);

    pointSize = 5.0;

    gl_PointSize = pointSize;

    //Parse the position of the point for all related fragments
    // -1 < pointCoord < 1
    pointCoord = position_MC.xy;
        
}
