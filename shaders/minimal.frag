#version 150

/*
 * Minimal Phong Shader - applies a fixed color to every rendered fragment
 * (C)opyright Hartmut Schirmacher, http://schirmacher.beuth-hochschule.de
 *
 */

uniform float time;

const float CELL_ALIVE = 1.0;   //(1,1,1 = White)
const float CELL_DEAD = 0.0;    //(0,0,0 = Black)

// uniform values from LifeMaterial
uniform vec2 dimension;
uniform sampler2D input_texture;
uniform int image_width;
uniform int image_height;

out vec4 outColor;      // final color of fragment

int cellNeighbors(vec2 uv)
{

    int neighbor = 0;

    // Calculate the step width in texture coordinates
    vec2 step = 1/dimension;

    // Count the neighbors
    if(texture(input_texture, uv + (step * vec2(-1,-1))).r == CELL_ALIVE)
    {
        ++neighbor;
    }
    if(texture(input_texture, uv + (step * vec2(0,-1))).r == CELL_ALIVE)
    {
        ++neighbor;
    }
    if(texture(input_texture, uv + (step * vec2(-1,1))).r == CELL_ALIVE)
    {
        ++neighbor;
    }
    if(texture(input_texture, uv + (step * vec2(1,-1))).r == CELL_ALIVE)
    {
        ++neighbor;
    }
    if(texture(input_texture, uv + (step * vec2(1,0))).r == CELL_ALIVE)
    {
        ++neighbor;
    }
    if(texture(input_texture, uv + (step * vec2(0,1))).r == CELL_ALIVE)
    {
        ++neighbor;
    }
    if(texture(input_texture, uv + (step * vec2(1,1))).r == CELL_ALIVE)
    {
        ++neighbor;
    }

    return neighbor;

}

vec3 calcNextState (vec2 uv)
{
    vec3 rgb_alive = vec3(CELL_ALIVE);
    vec3 rgb_dead = vec3(CELL_DEAD);
    vec3 color;

    int neighbors = cellNeighbors(uv);

    if (neighbors < 2)
         color = rgb_dead;
    else if (neighbors > 3)
         color = rgb_dead;
    else if (neighbors == 3)
         color = rgb_alive;
    else if (texture(input_texture, uv).r == CELL_ALIVE && neighbors == 2)
         color = rgb_alive;
    else
         color = rgb_dead;

    return color;
}


void main(void)
{
    // gl_FragCoord goes from 0 to dimension (window size)
    // uv needs to be 0 <= uv <= 1
    vec2 uv = gl_FragCoord.xy/dimension;
    outColor = vec4(calcNextState(uv),1.0);

}
