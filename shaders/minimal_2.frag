#version 150

/*
 * Minimal Phong Shader - applies a fixed color to every rendered fragment
 * (C)opyright Hartmut Schirmacher, http://schirmacher.beuth-hochschule.de
 *
 */

uniform float time;

// uniform values from LifeMaterial
uniform vec2 dimension;
uniform sampler2D input_texture;
uniform int image_width;
uniform int image_height;

out vec4 color;      // final color of fragment

in float pointSize;

in vec2 pointCoord;

// simple pseudo random number
float rand(vec2 xy){
    return fract(sin(dot(xy, vec2(12.9898,78.233))) * 43758.5453);
}

void main(void)
{
    // Calculate point in 0 to 1
    // Before: -1 < pointCoord < 1
    vec2 point = vec2(
                 (pointCoord.x + 1.0) / 2.0,
                 (pointCoord.y + 1.0) / 2.0
                );

    float rnd_r = rand(vec2(time)+point.x);
    float rnd_g = rand(vec2(time)+point.y);
    float rnd_b = 0.5;

    vec3 color_base = vec3(rnd_r,rnd_g,rnd_b);

    // Just draw the texture color
    color = vec4(texture(input_texture,point).rgb, 1);
    //if alive
    if (color == vec4(1,1,1,1))
    {
        color =vec4(color_base, 1.0);
    }
    else
    {
        discard;
    }
}
