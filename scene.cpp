#include "scene.h"

#include <iostream> // std::cout etc.
#include <assert.h> // assert()
#include <random>   // random number generation

#include <QtMath>
#include <QMessageBox>

#include "rtr_util.h"
#include "geometry/cube.h" // geom::Cube
#include "geometry/pointcloud.h" // geom::PointCloud
#include "material/phong.h"
#include "material/LifeMaterial.h"
#include <QRect>
#include <QFile>
#include <QDir>

using namespace std;

Scene::Scene(QWidget* parent) :
    QOpenGLFunctions(QOpenGLContext::currentContext()),
    parent_(parent),
    firstDrawTime_(clock_.now()),
    lastDrawTime_(clock_.now())
{
    //Deprecated stuff...
    auto phong_prog = createShaderProgram(":/shaders/phong.vert", ":/shaders/phong.frag");
    auto mtphong_prog = createShaderProgram(":/shaders/moreThanPhong.vert", ":/shaders/moreThanPhong.frag");


    // load shader source files and compile them into OpenGL program objects
    auto simulation_prog = createShaderProgram(":/shaders/minimal.vert", ":/shaders/minimal.frag");
    //auto simulation_prog = createShaderProgram(":/shaders/minimal_2.vert", ":/shaders/minimal_2.frag"); //DEBUG
    auto point_prog = createShaderProgram(":/shaders/minimal_2.vert", ":/shaders/minimal_2.frag");

    // point rendering materials
    sim_material_ = std::make_shared<LifeMaterial>(simulation_prog,1);
    point_material_ = std::make_shared<LifeMaterial>(point_prog,2);

    // Set dimensions
    point_material_->dimension = {100,100};
    sim_material_->dimension = {100,100};

    // Renderer for a texture
    render_life_texture = std::make_shared<GameOfLifeRenderer>(sim_material_);

    // load meshes from .obj files and assign shader programs to them
    meshes_["Duck"]    = make_shared<Mesh>(":/models/duck/duck.obj", point_material_);

    // HACK: try out point cloud rendering through back door
    meshes_["Duck"]->type_ = Mesh::PrimitiveType::POINTS;

    // make a point cloud
    std::vector<QVector3D> points;
    auto dimensionX = point_material_->dimension.x();
    auto dimensionY = point_material_->dimension.y();

    for( auto x = 0; x < dimensionX; x++)
    {
        float pointX = (float)x/dimensionX;
        for( auto y = 0; y < dimensionY; y++)
        {
            float pointY = (float)y/dimensionY;
            //Auf -1 < x < 1 strecken damit es besser aussieht
            points.push_back({pointX * 2 - 1,pointY * 2 - 1,0.0f});
        }
    }
    meshes_["Area"]   = make_shared<Mesh>(make_shared<geom::PointCloud>(points), point_material_, Mesh::PrimitiveType::POINTS);

    // pack each mesh into a scene node, scaled to standard size [1,1,1]
    nodes_["Area"]    = make_shared<Node>(meshes_["Area"],   Node::Transform::ScaleToOne);
    nodes_["Duck"]    = make_shared<Node>(meshes_["Duck"],   Node::Transform::ScaleToOne);

    // world contains the scene plus the camera
    nodes_["World"] = make_shared<Node>();

    // scene means everything but the main camera
    nodes_["Scene"] = make_shared<Node>();
    nodes_["World"]->children.push_back(nodes_["Scene"]);

    // add main camera node
    nodes_["Camera"] = make_shared<Node>();
    nodes_["World"]->children.push_back(nodes_["Camera"]);

    // add a light relative to the scene or world or camera
    nodes_["Light0"] = make_shared<Node>();
    lightNodes_.push_back(nodes_["Light0"]);

    // light attached to camera, placed right above camera
    nodes_["Camera"]->children.push_back(nodes_["Light0"]);
    nodes_["Light0"]->transformation.translate(QVector3D(0, 1, 0));

}

void Scene::createFBOs()
{
    auto dimension = sim_material_->dimension;

    // for high-res Retina displays
    auto pixel_scale = parent_->devicePixelRatio();

    // what kind of FBO do we want?
    auto fbo_format = QOpenGLFramebufferObjectFormat();
    fbo_format.setAttachment(QOpenGLFramebufferObject::Depth);

    // create FBOs for post processing
    // adapt size to the size of life
    fbo1_ = std::make_shared<QOpenGLFramebufferObject>(dimension.x(),
                                                       dimension.y(),
                                                       fbo_format);
    fbo2_ = std::make_shared<QOpenGLFramebufferObject>(dimension.x(),
                                                       dimension.y(),
                                                       fbo_format);
    qDebug() << "Creating FBOs with size" << fbo1_->size();

    sim_material_->initialTexture = std::make_shared<QOpenGLTexture>(QImage(":/start.bmp"));
    //Nearest Fileter is required to prevent averaging
    sim_material_->initialTexture->setMagnificationFilter(QOpenGLTexture::Nearest);
    sim_material_->initialTexture->setMinificationFilter(QOpenGLTexture::Nearest);

}

// this method is called implicitly when a redraw is required.
// the call goes rtrGLWidget.paintGL() -> SceneController.draw() -> Scene.draw()
void Scene::draw_()
{
    auto dimension = point_material_->dimension;

    // calculate animation time
    chrono::milliseconds millisec_since_first_draw;
    chrono::milliseconds millisec_since_last_draw;

    // calculate total elapsed time and time since last draw call
    auto current = clock_.now();
    millisec_since_first_draw = chrono::duration_cast<chrono::milliseconds>(current - firstDrawTime_);
    millisec_since_last_draw = chrono::duration_cast<chrono::milliseconds>(current - lastDrawTime_);
    lastDrawTime_ = current;

    // set time uniform in animated shader(s)
    float t = millisec_since_first_draw.count() / 1000.0f;
    point_material_->time = t;
    sim_material_->time = t;

    // create an FBO to render the scene into
    if(!fbo1_ || !fbo2_)
        createFBOs();

    //FBO1 : initiale texture in constructor
    qDebug() << "["<<t<<"] Draw simulated image.";
    // draw the actual game of life into texture
    fbo1_->bind();
    render_life_texture->draw(*fbo2_,QRect(0,0,dimension.x(),dimension.y()));
    fbo1_->release();

     point_material_->input_texture_id = fbo1_->texture();
     qDebug() << "["<<t<<"] Draw scene.";
    // draw the particles on screen
    draw_scene_();

    //Make the simulated to the input of the new one
    std::swap(fbo1_,fbo2_);
}

// set camera based on node position in scene graph
Camera
Scene::createCamera() {

    // derive aspect ratio from viewport size
    float aspect = float(parent_->width())/float(parent_->height());

    // create projection matrix based on 30 degrees field of view
    QMatrix4x4 projectionMatrix;
    projectionMatrix.perspective(30.0f,aspect,0.01f,1000.0f);

    // derive camera position from scene node world coordinates
    auto camToWorld = nodes_["World"]->toParentTransform(nodes_["Camera"]);
    auto viewMatrix = camToWorld.inverted();

    return Camera(viewMatrix, projectionMatrix);
}

void Scene::draw_scene_()
{
    Camera camera = createCamera();

    // clear buffer
    glClearColor(bgcolor_[0], bgcolor_[1], bgcolor_[2], 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // first light pass: standard depth test, no blending
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glDisable(GL_CULL_FACE);

    // point rendering: switch point size on
    glEnable(GL_PROGRAM_POINT_SIZE);

    // draw one pass for each light
    for(unsigned int i=0; i<lightNodes_.size(); i++) {

        // determine current light position in world coordinates
        QVector3D lightPosWC = nodes_["World"]->toParentTransform(lightNodes_[i]) * QVector3D(0,0,0);

        // set light position in material
        point_material_->lights[i].position_WC = lightPosWC;

        // draw light pass i
        nodes_["World"]->draw(camera, i);

        // settings for i>0 (add light contributions using alpha blending)
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE,GL_ONE);
        glDepthFunc(GL_EQUAL);
    }
}







