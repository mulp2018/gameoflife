#include "gameoflife_renderer.h"
#include "geometry/fullscreenrect.h"
#include "mesh/mesh.h"
#include "rtr_util.h"
#include "geometry/pointcloud.h" // geom::PointCloud

GameOfLifeRenderer::GameOfLifeRenderer(std::shared_ptr<LifeMaterial> mat):
    material_(mat)
{

    // make a point cloud
    std::vector<QVector3D> points;
    auto dimensionX = mat->dimension.x();
    auto dimensionY = mat->dimension.y();

    auto mesh = std::make_shared<Mesh>(std::make_shared<geom::FullScreenRect>(), mat);
    node_ = std::make_shared<Node>(mesh, Node::Transform::NoTransform);
}

void GameOfLifeRenderer::draw(QOpenGLFramebufferObject &fbo_in, QRect screen_mask)
{
    QMatrix4x4 view; // identity matrix by default
    QMatrix4x4 projection;
    projection.ortho(-1,1,-1,1,-1,1);
    Camera camera(view, projection);

    // query some OpenGL state to restore it later
    bool depth   = glIsEnabled(GL_DEPTH_TEST);
    bool cull    = glIsEnabled(GL_CULL_FACE);
    bool blend   = glIsEnabled(GL_BLEND);
    bool scissor = glIsEnabled(GL_SCISSOR_TEST);

    // initial state for drawing full-viewport rectangles
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glEnable(GL_SCISSOR_TEST);
    glScissor(screen_mask.left(),screen_mask.top(),
           screen_mask.width(),screen_mask.height());

    if(material_->input_texture_id == -1)
    {
        // use initial texture during rendering
        material_->bindTexture();
        qDebug() << "GameOfLifeRenderer: Set initial texture id to: "<<  material_->initialTexture->textureId();
        material_->input_texture_id = material_->initialTexture->textureId();
    }
    else
    {
        // use texture from fbo1 during rendering
        qDebug() << "GameOfLifeRenderer: Set texture id: "<< fbo_in.texture();
        material_->input_texture_id =  fbo_in.texture();
    }
    material_->image_size = QSize(material_->dimension.x(),material_->dimension.y());

    // draw full-screen rectangle with post processing material
    node_->draw(camera);

    // restore affected OpenGL state
    if(!scissor) glDisable(GL_SCISSOR_TEST);
    if(cull)     glEnable(GL_CULL_FACE);
    if(blend)    glEnable(GL_BLEND);
    if(depth)    glEnable(GL_DEPTH_TEST);
}

